Actor = class("Actor")

function Actor:initialize()
  self.turns = {}
  self.direction = 0
  self.orientation = math.pi/2
  self.speed = 0
  self.max_speed = 3
  self.speed_factor = 10
end


function Actor:keydown(dt)
  if self.dt_since_input > 0.1 then
    for key, m in pairs(self.inputs) do
      if love.keyboard.isDown(key) then
        if type(m) == 'function' then
          if self.dt_since_input > 0.5 then
            m(self, key)
            self.dt_since_input = 0
          end
        end
      end
    end
  end
  self.dt_since_input = self.dt_since_input + dt
end

function Actor:speedUp()
  self:speedChange(1)
end
function Actor:speedDown()
  self:speedChange(-1)
end

function Actor:speedChange(val)
  self.speed = self.speed + val
  if self.speed > self.max_speed then
    self.speed = self.max_speed
  elseif self.speed < - self.max_speed / 2 then
    self.speed = - self.max_speed / 2
  end
end

function Actor:turnLeft()
  self:turn(- math.pi / 4)
end

function Actor:turnRight()
  self:turn(math.pi / 4)
end

function Actor:turn(direction)
  if not self.turns then self.turns = {} end
  table.insert(self.turns, direction)
  self.orientation = (self.orientation + direction) % (2 * math.pi)
end


function Actor:update(dt)
  self:keydown(dt)
  if self.speed == 0 then
    return false
  end

  local old_position = {x = self.position.x, y = self.position.y}
  self.position.x = self.position.x + math.cos(self.orientation) * self.speed * dt * self.speed_factor
  self.position.y = self.position.y + math.sin(self.orientation) * self.speed * dt * self.speed_factor

  self.map:fitIntoMap(self.position)

  self:positionUpdated()
  return true
end

function Actor:positionUpdated()

end

